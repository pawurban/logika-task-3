#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string programToTest;

int fail_count = 0;

double maxWeight[] = {15.0, 20.0, 17.5};

//max good values of arguments
double Restrictions[] = {81.0,119.0,119.0,0,55.0,40.0,20.0,10.0,35.0,20.0,20.0};

void test(double Test[], const int n, double maxWeight) {
    int canFlyWithoutCharges = 1;
    string arguments;
    Restrictions[3] = maxWeight;
    
    if(maxWeight != 15.0 && maxWeight != 20.0)
        canFlyWithoutCharges = 2;
    else{ 
        for(int i = 0; i < n; i++)
              if(Test[i] > Restrictions[i]) {
              canFlyWithoutCharges = 0;
              }
    }
    
    //we add arguments from array to string
    for(int i = 0; i < n; i++) {
        arguments += " " + to_string(Test[i]);
    }

    FILE * file;
    
    //string executing the program
    string exec = "./" + programToTest + " " + to_string(maxWeight)+ arguments + " > /dev/null; echo $?";
    
    file = popen(exec.c_str(),"r");
    
    //if we cannot run the command
    if(file == NULL) {
        cout << "Could not run the command" << endl;
        exit(1);
    }
    
    char returnedValue = fgetc(file) - 48;	//we sub 48 to get int value
    
    pclose(file);

    if(canFlyWithoutCharges != returnedValue) {
        cout << "Program " + programToTest + " failed with arguments " + to_string(maxWeight)+ " "+ arguments << endl;
        fail_count++;
    }
    
}

//function checks  3 * 2^11 cases
void RunTheTest(double Test[], const int n, int i) {
    //if all things changed, we can test
    if(i >= n) {
        test (Test, n, maxWeight[0]);
        test (Test, n, maxWeight[1]);
        test (Test, n, maxWeight[2]);
        return;
    }
    
    Test[i] = Restrictions[i]+1.0; RunTheTest(Test,n,i+1);
    Test[i] = Restrictions[i]-1.0; RunTheTest(Test,n,i+1);
}

int main (int argc, char * argv[]) {

	if(argc == 2)
		programToTest = argv[1];
	else programToTest = "luggage";
        
	//if we cannot access the program
	ifstream infile(programToTest);
	
        if(!infile.good()) {
        cout << "Program to test is not accessible for tester" << endl; 
        return -1;
        }
        
	double Test[11];
        RunTheTest( Test, 11, 0 );
        cout << programToTest << " failed on " << fail_count << " tests" << endl;
        return 0;

}
