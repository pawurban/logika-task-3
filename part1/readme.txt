This is the first part of the project made for logic classes.

Program "luggage.c" checks whether a passenger can fly with Ryanair without additional charges.

It gets arguments: [weight of bought registered bag] [reg bag height] [reg bag width] [reg bag depth] [reg bag weight] [cabin bag height] [cabin bag width] [cabin bag depth] [cabin bag weight] *[small cabin bag height] *[small cabin bag width] *[small cabin bag depth].

* - optional arguments 

Program "luggage_negated.c" does the same thing but returns wrong answer, because it has negated logic condition.

Program "test.cpp" checks if program "luggage.c" or "luggage_negated.c" returns good value. It displays the number of tests program failed on.

//------------------------COMPILING INSTRUCTIONS------------------------//

To compile "luggage.c" simply use:

	gcc luggage.c -o luggage
	./luggage [arguments] 		to execute

To compile "luggage_negated.c" simply use:

	gcc luggage_negated.c -o luggage_negated
	./luggage_negated [arguments] 		to execute

To compile "test.cpp" use:

	g++ -std=c++11 test.cpp -o test
	./test [name of program to check (optional)]		to execute
