/*  Program gets arguments from the command line
    and tells whether a person has good size and weight
    of their luggage and can fly with Ryanair airlines without
    additional charges  */

#include <stdio.h>
#include <stdlib.h>

/*  IsInRegulations function checks the logical condition and answers the question:
    "Can you fly with Ryanair without additional charges?"
    It gets three tables (registered, big cabin, small cabin bag) and maxWeight of registered bag   */

int IsInRegulations(double reg[], double cab1[], double cab2[], double maxWeight){

    if(reg[0] > 81.0 || reg[1] > 119.0 || reg[2] > 119.0 || reg[3] > maxWeight ||
       cab1[0] > 55.0 || cab1[1] > 40.0 || cab1[2] > 20.0 || cab1[3] > 10.0 ||
       cab2[0] > 35.0 || cab2[1] > 20.0 || cab2[2] > 20.0 ) {
        return 1;
       }
       else {
        return 0;
       }
}

int main(int argc, char * argv[]){

    double reg[4], cab1[4], cab2[4];
    int i;
    double maxWeight;

    /* I put zeroes in cab2, because user does not have to check small cabin bag's size. */
    for(i = 0; i < 4; i++) {
        cab2[i] = 0.0;
    }

    if(argc < 10) {
        printf("X");
        return -1;
    }

    if((argc > 14) || (argc > 10 && argc < 13)) {
        printf("X");
        return -1;
    }

      if(argc == 10 || argc == 13 || argc == 14){

        maxWeight = atof(argv[1]);

        if(maxWeight == 15.0 || maxWeight == 20.0) {

            for(i = 2; i < 6; i++) {
                reg[i-2] = atof(argv[i]);
            }

            for(i = 6; i < 10; i++) {

                cab1[i-6] = atof(argv[i]);
            }

            if(argc == 13 || argc == 14) {
                for(i = 10; i < argc; i++) {
                cab2[i-10] = atof(argv[i]);
                }
            }
        }
        else {
            printf("Maximum weight of registered bag can only be 15 or 20 kilos according to bought option.\nAnother value was provided.\nProgram ends its execution.\n");
            return 2;
        }
    }
    i = IsInRegulations(reg,cab1,cab2,maxWeight);
    if(i == 1) {
	printf("According to data you gave, you can happily fly with Ryanair without additional charges.\n");
	return 0;
	}
    else printf("Unfortunately, Ryanair will charge you for wrong size or weight of your luggage.\n");
	return 1;
}


