This is second part of the task.

Program accepts as arguments names of files with definitions of logical functions (not, or, and, impl).

	./game --not [name of file describing not function] --and [and file] --or [or file] --impl [impl file]

Documents true.txt has some sentences of true value, false.txt - false value, unknown.txt - unknown logic value. There are essential for proper execution of the game. Do not change their names and their content.

//------------------------COMPILING INSTRUCTIONS-----------------------//

To compile game.cpp simply use:

	g++ game.cpp -o game

and

	./game --not not.txt --and and.txt --or or.txt --impl impl.txt

to run the game.
