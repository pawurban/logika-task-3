#include <iostream>
#include <cstring>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

//global arrays with definitions of logical functions
int Not[3][2];
int And[9][3];
int Or[9][3];
int Impl[9][3];

const int noOfSentences = 20;

//global arrays, helps not to repeat the same sentences in questions
bool trueS[noOfSentences];
bool falseS[noOfSentences];
bool unknownS[noOfSentences];

//struct represents one riddle
struct Riddle{
    string statement1;	//first statement
    string statement2;	//second statement
    short int answer;	//expected answer according to imported from files logic
};

//function returns false statement from file false.txt as string
string getfalseStatement() {
    fstream file;
    file.open("false.txt", ios::in);
    string data;
    
    short int tmp = rand()%noOfSentences;
    
    //if we get the number of sentence which was used we get another number
    while(falseS[tmp-1] == true)
        tmp = rand()%noOfSentences;
    
    getline(file,data);
    for(int i = 0; i < tmp-1; i++) {
        getline(file,data);
    }
    file.close();
    
    //sentence no tmp was used
    falseS[tmp-1] = true;
    
    string statement = data;
    return statement;
}

//function returns true statement from file true.txt as string
string gettrueStatement() {
    fstream file;
    file.open("true.txt", ios::in);
    string data;
    
    short int tmp = rand()%noOfSentences;
    
    //if we get the number of sentence which was used we get another number
    while(trueS[tmp-1] == true)
        tmp = rand()%noOfSentences;
    
    getline(file,data);
    for(int i = 0; i < tmp-1; i++) {
        getline(file,data);
    }
    file.close();
    
    trueS[tmp-1] = true;
    
    string statement = data;
    return statement;
}

//function returns unknown statement from file unknown.txt as string
string getunknownStatement() {
    fstream file;
    file.open("unknown.txt", ios::in);
    string data;
    
    short int tmp = rand()%noOfSentences;
    
    //if we get the number of sentence which was used we get another number
    while(unknownS[tmp-1] == true)
        tmp = rand()%noOfSentences;
    
    getline(file,data);
    for(int i = 0; i < tmp-1; i++) {
        getline(file,data);
    }
    file.close();
    
    unknownS[tmp-1] = true;
    
    string statement = data;
    return statement;
}

//function returns Riddle connected with logic "not"
Riddle getNotRiddle() {
    short int x = rand()%3;
    Riddle riddle;
    
    //depending on logic value we get sentence
    if(Not[x][0] == 0) riddle.statement1 = getunknownStatement();
    else if(Not[x][0] == 1)riddle.statement1 = gettrueStatement();
    else riddle.statement1 = getfalseStatement();
    
    riddle.answer = Not[x][1];
    
    return riddle;
}

//function returns Riddle (except from logic "not" riddle) 
//It needs int tab[][3] which is one of the global arrays with logic definitions
Riddle getRiddle(int tab[][3]) {
    //we choose number of line to create a riddle
    short int x = rand()%9;
    Riddle riddle;
    
    //depending on logic value we get sentences
    if(tab[x][0] == 0) riddle.statement1 = getunknownStatement();
    else if(tab[x][0] == 1)riddle.statement1 = gettrueStatement();
    else riddle.statement1 = getfalseStatement();

    if(tab[x][1] == 0) riddle.statement2 = getunknownStatement();
    else if(tab[x][1] == 1)riddle.statement2 = gettrueStatement();
    else riddle.statement2 = getfalseStatement();

    riddle.answer = tab[x][2];

    return riddle;
}

//rewrites file with definition of logical "not" to global array Not
// -1 represents false, 0 unknown, 1 true
void rewriteNotFile(char * NotFile) {
    fstream file;
    file.open(NotFile, ios::in);
    char data[5];
    
    for(int i = 0; i < 3; i++){
        file.getline(data, 5);
        for(int j = 0; j <= 2; j += 2){
            if(data[j] == '0')
                Not[i][j/2] = -1;
            else if(data[j] == '1')
                    Not[i][j/2] = 1;
            else Not[i][j/2] = 0;
        }
    }
    file.close();
}

//rewrites file with name given by char * to global array named tab
// -1 represents false, 0 unknown, 1 true
void rewriteOtherFile(char * File, int tab[][3]) {
    fstream file;
    file.open(File, ios::in);
    char data[7];
    for(int i = 0; i < 9; i++) {
        file.getline(data, 7);
        for(int j = 0; j <= 4; j += 2){
            if(data[j] == '0')
                tab[i][j/2] = -1;
            else if(data[j] == '1')
                tab[i][j/2] = 1;
            else if(data[j] == 'X')
                tab[i][j/2] = 0;
        }
    }
    file.close();
}

//checks if the answer is correct
bool correct(string x, short int answ){
    if((x == "1" && answ == 1)|| ((x == "X" || x == "x") && answ == 0) || (x == "0" && answ == -1))
        return true;
    else return false;
}

//displays the correct answer 
void displayCorrectAnsw(Riddle riddle){
    string x;
    if(riddle.answer == 1) x = "1";
    else if(riddle.answer == -1) x = "0";
    else x = "X";
    cout << "Correct answer: " << x << ".\n";
}

//inserts false in global bool arrays
void falseGlobalArrays() {
    for(int i = 0; i < noOfSentences; i++)
        trueS[i] = falseS[i] = unknownS[i] = false;
}

//displays that the name of file was not given and returns -1
void FileNotGiven() {
    cout << "The name of file is not given!\n";
}

void displaySentences(Riddle riddle) {
    cout << "p: " << riddle.statement1 << "\n";
    cout << "q: " << riddle.statement2 << "\n";
}

//asks a question and returns riddle
Riddle askAQuestion(short int x) {
		Riddle riddle;
	    if(x == 0) {
                riddle = getNotRiddle();
                cout << "p: " << riddle.statement1 << "\n";
                cout << "What is logical value of this sentence: !p ?: ";
            }
            else if(x == 1){
                riddle = getRiddle(And);
                displaySentences(riddle);
                cout << "What is logical value of this sentence: p ^ q ?: ";
            }
            else if(x == 2){
                riddle = getRiddle(Or);
                displaySentences(riddle);
                cout << "What is logical value of this sentence: p v q ?: ";
            }
            else if(x == 3) {
                riddle = getRiddle(Impl);
                displaySentences(riddle);
                cout << "What is logical value of this sentence: p => q ?: ";
	    }
return riddle;
}

//function which realize the game and interractions with player
void play() {
    cout << "\nWelcome in the game \"Polish logicians\"!" << endl;
    cout << "To win, you will have to answer 10 questions." << endl;
    cout << "Every single question has one or two sentences and logical conjunction." << endl;
    cout << "Your job is to guess the logical value of sentence or sentences\n (0-false, 1-true or X-unknown, when we cannot say exact logical value or when it varies)" << endl;
    cout << "When you do this, it is time to guess the value of the sentence with logical conjunction." << endl;
    cout << "If you get it right I will give you 1 point. If not, I will take one." << endl;
    cout << "If you use another character except from 0, 1, x or X, you lose 1 point." << endl;
    cout << "To win you need to get only 8 points." << endl;
    cout << "Ready? (Y/N)  ";
    string x;			
    cin >> x;
    int points = 0;
    if(x == "N" || x == "n") {
        cout << "No is no. Bye!:)";
        return;
    }
    else if(x != "Y" && x != "y") {
        cout << "I see, that this game is too hard for you. You couldn't answer that easy question? Good bye.\n";
        return;
    }
    else {
        cout <<"\nOK, then. Let the fun begin!\nGood luck!\n";
        
        for(int i = 1; i <= 10; i++) {
            short int x = rand()%4;
            cout << "\n//------QUESTION " << i << "------//\n";
		Riddle riddle = askAQuestion(x);
            
	   	string answ;
                cin >> answ;
                short int expectedanswer = riddle.answer;
                if(correct(answ, expectedanswer)) {
                    points += 1;
                    cout << "Well done! Correct answer! You get 1 point.\n";
                    cout << "Actual number of points: " << points << ".\n";
                    if(i != 10) cout << "\nHere is the next question:\n";
                }
		else {
			points -= 1;
			cout << "Unfortunately, it is not correct answer. You lose 1 point.\n";
                        displayCorrectAnsw(riddle);
			cout << "Actual number of points: " << points << ".\n";
		}
               
        }
        cout << "\nIt was the last question in our game!\n\n";
        
        if(points >= 8) {
                cout << "Well done! You won! \nI am so proud of you! You are logic master!\n";
                cout << "Here is medal for you!\n";
                cout << "     _______________\n    |@@@@|     |####|\n    |@@@@|     |####|\n    |@@@@|     |####|\n    \\@@@@|     |####/\n";
                cout << "     \\@@@|     |###/\n      `@@|_____|##'\n           (O)\n        .-'''''-.\n      .'  * * *  `.\n";
                cout << "     :  *       *  :\n    : ~ L O G I C ~ :\n    : ~M A S T E R~ :\n     :  *       *  :\n      `.  * * *  .'\n        `-.....-'\n\n";
        } 
        else cout << "Unfortunately, " << points << " pts is not enough to win the game.\nTry once more!\n";
            
    }     
}

int main(int argc, char * argv[]) {
    srand(time(NULL));
    char * NotFile, * OrFile, * ImplFile, * AndFile;

//parser linii komend, zapisuje nazwy plikow do zmiennych
    if(argc < 9) {
        cout << "There are not all files given! Too few arguments!\n";
    }
    else {
        for(int i = 1; i < argc; i += 2) {
            if(strcmp(argv[i],"--not") == 0)
                if(i + 1 <= argc - 1) {
                    NotFile = argv[i+1];
                }
                else{ 
                    FileNotGiven();
                    return -1;
                }
            else if(strcmp(argv[i],"--or") == 0)
                if(i + 1 <= argc - 1) {
                    OrFile = argv[i+1];
                }
                else { 
                    FileNotGiven();
                    return -1;
                }
            else if(strcmp(argv[i],"--and") == 0)
                if(i + 1 <= argc - 1) {
                    AndFile = argv[i+1];
                }
                else { 
                    FileNotGiven();
                    return -1;
                }
            else if(strcmp(argv[i],"--impl") == 0)
                if(i + 1 <= argc - 1) {
                    ImplFile = argv[i+1];
                }
                else { 
                    FileNotGiven();
                    return -1;
                }
            else cout << "Wrong option!\n";
        }
    }

    falseGlobalArrays();
    rewriteNotFile(NotFile);
    rewriteOtherFile(AndFile,And);
    rewriteOtherFile(OrFile,Or);
    rewriteOtherFile(ImplFile,Impl);
    play();

    return 0;
}
